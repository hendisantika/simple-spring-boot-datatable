package com.example.demo;

import com.example.demo.entity.Contact;
import com.example.demo.repository.ContactRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

    @Bean
    CommandLineRunner initialize(ContactRepository repo) {
        return (args) -> {
            repo.save(new Contact(1L, "Uzumaki Naruto", "081321411661", "uzumaki_naruto@konohagakure.com", "Konoha"));
            repo.save(new Contact(2L, "Uchiha Sasuke", "081321411662", "uchiha_sasuke@konohagakure.com", "Konoha"));
            repo.save(new Contact(3L, "Sakura Haruno", "081321411663", "sakura_haruno@konohagakure.com", "Konoha"));
            repo.save(new Contact(4L, "Hatake Kakashi", "081321411664", "hatake_kakashi@konohagakure.com", "Konoha"));
            repo.save(new Contact(5L, "Namikaze Minato", "081321411665", "namikaze_minato@konohagakure.com", "Konoha"));
            repo.save(new Contact(6L, "Sabaku No Gaara", "081321411666", "gaara_imut@konohagakure.com", "Suna"));
            repo.save(new Contact(7L, "Kankuro", "081321411667", "kankuro@konohagakure.com", "Suna"));
            repo.save(new Contact(8L, "Temari", "081321411668", "temari@konohagakure.com", "Suna"));
        };
    }
}