package com.example.demo.controller;

import com.example.demo.entity.Contact;
import com.example.demo.repository.ContactRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : simple-spring-boot-datatable
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/09/18
 * Time: 20.58
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/api/contacts")
public class RestContactController {

    private Logger log = LogManager.getLogger(RestContactController.class);

    @Autowired
    ContactRepository repo;

    @GetMapping
    List<Contact> getAllContacts() {
        return repo.findAll();
    }

    @PostMapping("")
    ResponseEntity<Object> saveCandidate(@RequestBody Contact contact) {
        if (contact != null) {
            return new ResponseEntity<>(repo.save(contact), HttpStatus.OK);
        } else {

            return new ResponseEntity<>("Data Null", HttpStatus.OK);
        }

    }

    @GetMapping("/findById")
    Optional<Contact> findContactById(@RequestParam(value = "id") Long id) {
        return repo.findById(id);

    }

    @GetMapping("/{id}/find")
    Optional<Contact> findById(@PathVariable(value = "id") Long id) {
        return repo.findById(id);
    }

    @DeleteMapping("/{id}/delete")
    void deleteContact(@PathVariable(value = "id") Long id) {
        Optional<Contact> hasil = repo.findById(id);

        hasil.ifPresent(
                hasil1 -> {
                    log.info("Datanya ada ! --> {} ", hasil.toString());
                }
        );

        Contact result = hasil.orElse(null);
        repo.deleteById(id);

    }
}
