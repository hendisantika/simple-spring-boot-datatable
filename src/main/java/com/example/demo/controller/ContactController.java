package com.example.demo.controller;

import com.example.demo.entity.Contact;
import com.example.demo.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : simple-spring-boot-datatable
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/09/18
 * Time: 21.10
 * To change this template use File | Settings | File Templates.
 */

@Controller
@RequestMapping
public class ContactController {
    @Autowired
    ContactRepository repo;

    @GetMapping
    String index(Model m) {
        Iterable<Contact> contacts = repo.findAll();

        m.addAttribute("contacts", contacts);
        m.addAttribute("contact", new Contact());

        return "index";
    }

    @PostMapping(value = "/save")
    public String prosesForm(@Valid Contact c, BindingResult errors,
                             HttpSession session) {
        if (errors.hasErrors()) {
            return "/contacts/";
        }

        repo.save(c);

        return "redirect:/contacts/";
    }

    @GetMapping("/edit/{id}")
    String editCandidate(@PathVariable(value = "id", required = false) Long id, Model m) {
        if (id != null) {
            Optional<Contact> c = repo.findById(id);
            if (c != null) {
                m.addAttribute("c", c);
            }
        }
        m.addAttribute("candidate", new Contact());
        return "edit";
    }

    @GetMapping("/delete/{id}")
    String deleteCandidate(@PathVariable(value = "id", required = false) Long id, Model m) {
        if (id != null) {
            Optional<Contact> c = repo.findById(id);
            if (c != null) {
                m.addAttribute("c", c);
            }
        }
        repo.deleteById(id);

        return "redirect:/contacts/";
    }
}
