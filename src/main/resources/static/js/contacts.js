$(document).ready(function () {
    var table = $('#contacts').DataTable({
        "sAjaxSource": "/api/contacts",
        "sAjaxDataProp": "",
        "order": [[0, "asc"]],
        "columns": [
            {"data": "id"},
            {"data": "name"},
            {"data": "phone"},
            {"data": "email"},
            {"data": "address"},
            {
                "data": "id",
                "render": function (data, type, full, meta) {
                    return '<a class="btn btn-danger btn-sm" onclick="javascript:return confirm(\'Anda yakin menghapus product ini ?\');" href="/api/contacts/{id}/delete/' + data + '">Delete</a>';
                }
            }
        ]
    });
});